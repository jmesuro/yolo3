
#tensorflow-yolo-v3
Implementation of YOLO v3 object detector in Tensorflow (TF-Slim). Full tutorial can be found [here](https://medium.com/@pawekapica_31302/implementing-yolo-v3-in-tensorflow-tf-slim-c3c55ff59dbe).

Original source: https://github.com/mystic123/tensorflow-yolo-v3

Yo lo corrí en python 3.6.